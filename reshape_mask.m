function [X]=reshape_mask(r,mask)
Ind=find(mask);
if size(r,2)==1
       X=zeros(size(double(mask)));
        X=unfold(X);
        X(Ind,:)=r;
        X=reshape(X,size(double(mask)));
else
    X=zeros(size(double(mask),1),size(double(mask),2),size(r,2));
    X=unfold(X);
    X(Ind,:)=r;
    X=reshape(X,size(double(mask),1),size(double(mask),2),size(r,2));
    end
end
