function spe=extract_spe(im,mask)
Ind=find(mask);
im_f=unfold(im);
spe=im_f(Ind,:);
end