function plot_muti_spectra(W,spe,lb_tx)
% figure,
tmp=spe(1,:);
camp=colormap(jet(size(spe,1)));
camp(ismember(camp,[1,1,0],'rows'),:)=[1,0.5,0.5];
plot(W,tmp,'color',camp(1,:),'Linewidth',2);hold on,
set(gcf,'color','w');
text(W(end),120,lb_tx{1,1},'Color',camp(1,:),...
    'Fontweight','bold','Fontsize',17)
for i=2:size(spe,1)
 tmp=spe(i,:);
 tmp=tmp+(i-1)*120;
 plot(W,tmp,'color',camp(i,:),'Linewidth',3);  
  text(W(end),i*120,lb_tx{1,i},'Color',camp(i,:),...
     'Fontweight','bold','Fontsize',17)
end
xlim([W(1)-30 W(end)+30]), ylim([-10 i*120+40])
set ( gca, 'xdir', 'reverse' ), 
set(gca,'FontSize',19),box on
set(gca,'ytick',[]); xlabel('Wavenumber (cm^{-1})');






