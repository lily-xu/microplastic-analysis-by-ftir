function [L,S,Ev_per]=PCA_im(im,W,mask)
if nargin==2
    mask=ones(size(im,1),size(im,2));
end
spe=extract_spe(im,mask);
[L,S,Ev_per]=PCA_spe(spe);
S=reshape_mask(S,mask);
figure
set(gcf,'color','w');
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0.1, 0.25, 0.7, 0.45]);
for i=1:3
    subplot(2,3,i)
    [r1 r2]=percent(S(:,:,i),mask);
    imagesc(S(:,:,i),[r1 r2])
    colorbar,colormap(jet)
    axis off
    xt = get(gca, 'XTick');
    set(gca, 'FontSize', 16)
    title(['PC' num2str(i) '-' num2str(Ev_per(i),2) '%'])   
    subplot(2,3,i+3)
    plot(W,L(:,i),'k','LineWidth',2);
    set ( gca, 'xdir', 'reverse' )
    xlabel('Wavenumber (cm^{-1})','Fontsize',17);
    xt = get(gca, 'XTick');
    set(gca, 'FontSize', 16)
    axis tight
    title(['Loading' num2str(i)])
end
end

function spe=extract_spe(im,mask)
Ind=find(mask);
im_f=unfold(im);
spe=im_f(Ind,:);
end

function [X]=reshape_mask(r,mask)
Ind=find(mask);
if size(r,2)==1
       X=zeros(size(double(mask)));
        X=unfold(X);
        X(Ind,:)=r;
        X=reshape(X,size(double(mask)));
else
    X=zeros(size(double(mask),1),size(double(mask),2),size(r,2));
    X=unfold(X);
    X(Ind,:)=r;
    X=reshape(X,size(double(mask),1),size(double(mask),2),size(r,2));
    end
end
function [L,S,Ev_per]=PCA_spe(X)
X=bsxfun(@minus,X,mean(X));
Xcov=cov(X);
[U,Sev,V]=svd(Xcov);
L=V;
S=X*L;
Ev=diag(Sev);
Ev_per=100*Ev/sum(Ev);
end

function [r1,r2]=percent(img,mask,p)
if nargin==2
    p=98;
end
im_f=unfold(img);
im_f=im_f(find(mask),:);
   r2=prctile(im_f,p);
   r1=prctile(im_f,100-p);
end

