function[X]=SNV(X)
if ndims(X)==2
Xmean=mean(X,2);
Xstd=std(X,0,2);
X=(bsxfun(@minus,X,Xmean));
X=(bsxfun(@rdivide,X,Xstd));
X(isnan(X))=0;
elseif ndims(X)==3
X_uf=unfold(X);
Xmean=mean(X_uf,2);
Xstd=std(X_uf,0,2);
X_uf=(bsxfun(@minus,X_uf,Xmean));
X_uf=(bsxfun(@rdivide,X_uf,Xstd));
X_uf(isnan(X_uf))=0;
X=reshape(X_uf,size(X));
end
end