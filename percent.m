function [r1,r2]=percent(img,mask,p)
if nargin==2
    p=98;
end
im_f=unfold(img);
im_f=im_f(find(mask),:);
   r2=prctile(im_f,p);
   r1=prctile(im_f,100-p);
end