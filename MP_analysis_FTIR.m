% 
%   Author: Junli Xu. 
%   Adviser: Aoife A Gowen
%   Email:  junli.xu@ucdconnect.ie

%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

clear all,
clc,
addpath(genpath('C:\...'))  %Add a folder and its subfolders to the search path
load MP_analysis_example % Import dataset

% Inspect the complete information about structure of Data
Data
% Extract the wavenumber variables from the structure of Data
W=Data.W; 

%% Creat spectral library and show the reference spectra
MP_label={'PE','PC','PVC','PET','PSu','PS','PP','PTFE'}; % The categories of all plastics

% Using for loop to extract spectra of all plastics
MP_ref=[]; % Reference spectra from all plastics
MP_ref_mean=[];% Reference mean spectra from all plastics
MP_ref_index=[]; % The index label (number from 1 to 8) for each plastic, 
% ++++++++++++++++++indicating different classes

for i=1:8    
MP_ref=[MP_ref;eval(strcat('Data.',MP_label{1,i}))];
MP_ref_index=[MP_ref_index;i*ones(16,1)];
MP_ref_mean(i,:)=mean(eval(strcat('Data.',MP_label{1,i})));
end
% Show plastic reference spectra
figure,plot_muti_spectra(W,MP_ref_mean,MP_label) % Pre-programmed function to show multiple spectra

%% Inspect Ground truth and plot mean spectra of MP
GT=Data.GT; % Extract Ground truth from Data structure
Spe_selected_coordinate=Data.Spe_selected_coordinate; % Extract the coordinates from three selected pixels 
Spe_mean_selected=Data.Spe_mean_selected; % Extract mean spectra of three selected pixels from each particle

% Assign a label for each particle
Particle_label={'P1';'P2';'P3';'P4';'P5';'P6';'P7'};
% Create a colormap which will assign a color to each particle
camp=colormap(jet(7));
camp(ismember(camp,[1,1,0],'rows'),:)=[1,0.5,0.5]; 
camp=[[0,0,0];camp];
close all

% Show Ground truth image and plot mean spectra
figure,
set(gcf,'color','w'); % Set background as white
ax2=subplot(1,2,2)
plot_muti_spectra(W,Spe_mean_selected,Particle_label') % Pre-programmed function to show multiple spectra
title('Mean selected spectra','Fontsize',17);
ax1=subplot(1,2,1)
imagesc(GT),hold on, % Show ground truth image
scatter(Spe_selected_coordinate(:,1),Spe_selected_coordinate(:,2),100,'x',...
    'MarkerEdgeColor',[0 0 0]) % The selected pixels are marked with x
axis off, 
colormap(ax1,camp); % Assign color to each particle
title('Ground truth','Fontsize',17);

%% PCA multivariate analysis on the mixture dataset
im=Data.Mix; % Extract mixture dataset from Data structure
[L,S,Ev_per]=PCA_im(im,W); % Pre-programmed function to apply PCA on 3-D spectral datacube
%%%% L is the loading, S is the score image, Ev_per is variance explained
%%%% by each PC

%% Spectral matching using pearson correlation
HQI_Correlation=corr(Spe_mean_selected',MP_ref_mean'); 
% Use corr function to compute correlation coefficients

% Show correlation coefficients in the form of Table
array2table(HQI_Correlation,'RowNames',Particle_label,'VariableNames',MP_label)

%% Model_based classification
%%% Step 1: Classification of MP and non-MP
Mesh=Data.Mesh_spe; % Extract spectra from metal mesh
X=[MP_ref;Mesh]; % Generate the spectral library as independent variable
Y_two=[ones(size(MP_ref_index,1),1);zeros(size(Mesh,1),1)]; % Two classes:
%%% 1's indicate MP, while 0's represent non-MP

Mdl_two = fitcecoc(X,Y_two,'Coding','onevsall');% Model development
Pred_img_two =reshape(predict(Mdl_two,unfold(im)),size(im(:,:,1))); % Apply
%%% model on the mixture image, first unfold and then re-fold
Mask_MP=Pred_img_two; % Creat a mask to remove non-MP regions

%%% Step 2: Classification of different MP categories, from raw spectra
Mdl_MP = fitcecoc(MP_ref,MP_ref_index,'Coding','onevsall'); %Model development
Pred_img_MP =reshape_mask(predict(Mdl_MP,extract_spe(im,Mask_MP)),Mask_MP);%
%%% Apply model on the mixture image, first extract spectra from the MP
%%% area using pre-programmed extract_spe function, and finally re-fold using 
%%% reshape_mask function to generate classification map. 
Pred_spe =predict(Mdl_MP,Spe_mean_selected); % Apply model on the mean spectra


%%% Step 2: Classification of different MP categories, from pre-processed spectra
MP_ref_SNV_SG=SNV(savgol(MP_ref,35,3,0)); % Preprocessing using Savitzky-Golay smoothing
%%% and SNV
Mdl_MP_SNV_SG = fitcecoc(MP_ref_SNV_SG,MP_ref_index,'Coding','onevsall'); % Model development
Pred_img_MP_SNV_SG =reshape_mask(predict(Mdl_MP_SNV_SG,SNV(savgol...
    (extract_spe(im,Mask_MP),35,3,0))),Mask_MP); 
%%% Apply model on the mixture image, first extract spectra from the MP
%%% area using pre-programmed extract_spe function, preprocessing, model prediction
%%% and finally re-fold using reshape_mask function to generate classification map. 
Pred_spe_SNV_SG =predict(Mdl_MP_SNV_SG,SNV(savgol(Spe_mean_selected,35,3,0)));% Apply model on the mean spectra

%% Apply pearson correlation on every pixel
%%%Apply correlation on the mixture image, first extract spectra from the
%%%MP pixels
Correlation_pixel=corr(Spe_mean_selected',extract_spe(im,Mask_MP)'); 
%%%Assign the class based on the maximun correlation coefficients
[~,HQI_Correlation_map]=max(Correlation_pixel);
%%%Re-fold using reshape_mask function to generate identification map
HQI_Correlation_map=reshape_mask(HQI_Correlation_map',Mask_MP);

%% Comparison based on classfication maps
% Misclassified pixels are indicated with 'x' marks 
figure,
set(gcf,'color','w');
subplot(2,2,1)
imagesc(Pred_img_two);
axis off,colormap(camp);
title('MP and non-MP','Fontsize',17);
h=subplot(2,2,2)
imagesc(Pred_img_MP);
hold on, axis off,colormap(h,[camp;[1 1 0]]);
mask_misclass_raw=(Pred_img_MP-GT)~=0;
B = regionprops(mask_misclass_raw,'PixelList');
mis_pixel=[];
for i=1:size(B,1)
mis_pixel=[mis_pixel;B(i).PixelList];
end
scatter(mis_pixel(:,1),mis_pixel(:,2),90,'kx')
title('Classification map: Raw','Fontsize',17);
subplot(2,2,3)
imagesc(Pred_img_MP_SNV_SG);hold on, 
mask_misclass_raw=(Pred_img_MP_SNV_SG-GT)~=0;
B = regionprops(mask_misclass_raw,'PixelList');
mis_pixel=[];
for i=1:size(B,1)
mis_pixel=[mis_pixel;B(i).PixelList];
end
scatter(mis_pixel(:,1),mis_pixel(:,2),90,'kx')
axis off,colormap(camp);
title('Classification map: SNV','Fontsize',17);
subplot(2,2,4)
imagesc(HQI_Correlation_map);
hold on, 
mask_misclass_raw=(HQI_Correlation_map-GT)~=0;
B = regionprops(mask_misclass_raw,'PixelList');
mis_pixel=[];
for i=1:size(B,1)
mis_pixel=[mis_pixel;B(i).PixelList];
end
scatter(mis_pixel(:,1),mis_pixel(:,2),90,'kx')
axis off,colormap(camp);
title('Classification map:Correlation','Fontsize',17);

%% Comparison based on confusion matrix 
%%% Show confusion matrix
YTrue=extract_spe(GT,Mask_MP); % Extract true class from each pixels
YTrue=num2str(YTrue); % Convert to string
YTrue=cellstr(YTrue);% Convert to cell array
% Replace number with plastic categories
YTrue = strrep(YTrue,'1','PE');
YTrue = strrep(YTrue,'2','PC');
YTrue = strrep(YTrue,'3','PVC');
YTrue = strrep(YTrue,'4','PET');
YTrue = strrep(YTrue,'5','PSu');
YTrue = strrep(YTrue,'6','PS');
YTrue = strrep(YTrue,'7','PP');

% Replace number with plastic categories for correlation map
YPred_Corr=extract_spe(HQI_Correlation_map,Mask_MP);
YPred_Corr=num2str(YPred_Corr);
YPred_Corr=cellstr(YPred_Corr);
YPred_Corr = strrep(YPred_Corr,'1','PE');
YPred_Corr = strrep(YPred_Corr,'2','PC');
YPred_Corr = strrep(YPred_Corr,'3','PVC');
YPred_Corr = strrep(YPred_Corr,'4','PET');
YPred_Corr = strrep(YPred_Corr,'5','PSu');
YPred_Corr = strrep(YPred_Corr,'6','PS');
YPred_Corr = strrep(YPred_Corr,'7','PP');
% Replace number with plastic categories for classification map obtained
% from raw spectra
YPred_raw=extract_spe(Pred_img_MP,Mask_MP);
YPred_raw=num2str(YPred_raw);
YPred_raw=cellstr(YPred_raw);
YPred_raw = strrep(YPred_raw,'1','PE');
YPred_raw = strrep(YPred_raw,'2','PC');
YPred_raw = strrep(YPred_raw,'3','PVC');
YPred_raw = strrep(YPred_raw,'4','PET');
YPred_raw = strrep(YPred_raw,'5','PSu');
YPred_raw = strrep(YPred_raw,'6','PS');
YPred_raw = strrep(YPred_raw,'7','PP');
YPred_raw = strrep(YPred_raw,'8','PTFE');
% Replace number with plastic categories for classification map obtained
% from SNV pre-processed spectra
YPred_SNV=extract_spe(Pred_img_MP_SNV_SG,Mask_MP);
YPred_SNV=num2str(YPred_SNV);
YPred_SNV=cellstr(YPred_SNV);
YPred_SNV = strrep(YPred_SNV,'1','PE');
YPred_SNV = strrep(YPred_SNV,'2','PC');
YPred_SNV = strrep(YPred_SNV,'3','PVC');
YPred_SNV = strrep(YPred_SNV,'4','PET');
YPred_SNV = strrep(YPred_SNV,'5','PSu');
YPred_SNV = strrep(YPred_SNV,'6','PS');
YPred_SNV = strrep(YPred_SNV,'7','PP');

% Show confusion matrix and disclassification maps
figure,subplot(2,2,1)
confusionchart(YTrue,YPred_raw,'ColumnSummary','column-normalized',...
    'RowSummary','row-normalized','Title','Validation');
set(gca,'Fontsize',15),
acc_raw=sum(extract_spe(Pred_img_MP,Mask_MP) == extract_spe(GT,Mask_MP))./numel(YTrue)
title(['Raw model (CCR=' num2str(acc_raw*100,2) '%)'])
subplot(2,2,2)
confusionchart(YTrue,YPred_SNV,'ColumnSummary','column-normalized',...
    'RowSummary','row-normalized','Title','Validation');
set(gca,'Fontsize',15)
acc_raw=sum(extract_spe(Pred_img_MP_SNV_SG,Mask_MP) == extract_spe(GT,Mask_MP))./numel(YTrue)
title(['SNV model (CCR=' num2str(acc_raw*100,2) '%)'])
subplot(2,2,3)
confusionchart(YTrue,YPred_Corr,'ColumnSummary','column-normalized',...
    'RowSummary','row-normalized','Title','Validation');
set(gca,'Fontsize',15)
acc_raw=sum(extract_spe(HQI_Correlation_map,Mask_MP) == extract_spe(GT,Mask_MP))./numel(YTrue)
title(['Correlation (CCR=' num2str(acc_raw*100,2) '%)'])


